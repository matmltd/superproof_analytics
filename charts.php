<?php
/**
 * Created by PhpStorm.
 * User: jake
 * Date: 26/09/2017
 * Time: 10:34
 */

function trafficChart ($chartID){

    $visitors = get_visitors();

    if(!empty($visitors)) {

        $array = "";
        foreach ($visitors as $date => $visits){
            $array .= ",['" . substr($date, 0, 4) . "/" . substr($date, 4, 2) . "/" . substr($date, 6, 2) . "'," . $visits . "]";
        }

        $html = "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(trafficChart);

      function trafficChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Visitors']
          " . $array . "
        ]);

        var options = {
          title: 'Visitors',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "')); 
        chart.draw(data, options);
      }
    </script>";

        return $html;
    } else {
        return "";
    }
}

function techChart ($chartID){

    $browsers = get_tech();

    if(!empty($browsers)) {

        $array = "";
        ksort($browsers);
        foreach ($browsers as $browser => $visits){
            //if()
            $array .= ",['" .$browser . "'," . $visits . "]";
        }

        $html = "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(techChart);

      function techChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Visitors']
          " . $array . "
        ]);

        var options = {
          title: 'Browsers',
          is3D: true,
          sliceVisibilityThreshold: 1/50
        };

        var chart = new google.visualization.PieChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

        return $html;
    } else {
        return "";
    }
}

function costPerConversionChart($chartID){
    $costs = get_conversion_rates();

    if(!empty($costs)) {

        $array = "";
        foreach ($costs as $date => $cost){
            $array .= ",['" . substr($date, 0, 4) . "/" . substr($date, 4, 2) . "/" . substr($date, 6, 2) . "'," . $cost . "]";
        }

        $html = "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(costPerConversionChart);

      function costPerConversionChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Cost']
          " . $array . "
        ]);

        var options = {
          title: 'Cost Per Conversion - Overall Average: £" . round(get_conversion_average(), 2) . "',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

        return $html;
    } else {
        return "";
    }

}

function ClicksChart($chartID) {


	$clicks = get_clicks();

	if(!empty($clicks)) {

		$array = "";
		$count = 0;
		$clicksTotal = 0;
		foreach ($clicks as $date => $click){
			$array .= ",['" . substr($date, 0, 4) . "/" . substr($date, 4, 2) . "/" . substr($date, 6, 2) . "'," . $click . "]";
			$count++;
			$clicksTotal = $clicksTotal + $click;
		}

		$html = "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(ClicksChart);

      function ClicksChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Clicks']
          " . $array . "
        ]);

        var options = {
          title: 'Adwords Clicks - Total: " . round($clicksTotal, 2) . " -  Average per day: " . round($clicksTotal / $count, 2) .  "',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {title: 'Clicks',minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "')); 
        chart.draw(data, options);
      }
    </script>";

		return $html;
	} else {
		return "";
	}
}

function SpendChart($chartID) {


	$clicks = get_spend();

	if(!empty($clicks)) {

		$array = "";
		$count = 0;
		$clicksTotal = 0;
		foreach ($clicks as $date => $click){
			$array .= ",['" . substr($date, 0, 4) . "/" . substr($date, 4, 2) . "/" . substr($date, 6, 2) . "'," . $click . "]";
			$count++;
			$clicksTotal += $click;
		}

		$html = "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(SpendChart);

      function SpendChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Cost']
          " . $array . "
        ]);

        var options = {
          title: 'Adwords Spend - Total: £" . round($clicksTotal, 2) . " - Average per day: £" . round($clicksTotal / $count, 2). " ',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {title: 'Cost (£)',minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "')); 
        chart.draw(data, options);
      }
    </script>";

		return $html;
	} else {
		return "";
	}
}

function ConversionsChart($chartID) {


	$conversions = get_conversions();

	if(!empty($conversions)) {

		if (isset($_GET['s'])) {
			$start = date('Y-m-d', $_GET['s']);
		} else {
			$start = date('Y-m-d', strtotime('-30 days'));
		}

		if (isset($_GET['e'])) {
			$end = date('Y-m-d', $_GET['e']);
		} else {
			$end = date('Y-m-d');
		}

		$array = "";
		$count = 0;
		$clicksTotal = 0;
		foreach ($conversions as $year => $month){
			foreach($month as $day => $conversions) {
				foreach ($conversions as $conversion) {
					if($conversion['time'] >= strtotime($start) && $conversion['time'] <= strtotime($end)) {
						$array = ",['" . date( 'Y/m/d', $conversion['time'] ) . "'," . $conversion['count'] . "]" . $array;
						$count ++;
						$clicksTotal += $conversion['count'];
					}
				}
			}
		}

		$html = "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(ConversionsChart);

      function ConversionsChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Conversions']
          " . $array . "
        ]);

        var options = {
          title: 'Conversions (Forms Submissions) - Total: " . round($clicksTotal, 2) . " - Average per day: " . round($clicksTotal / $count, 2). " ',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {title: 'Conversions',minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "')); 
        chart.draw(data, options);
      }
    </script>";

		return $html;
	} else {
		return "";
	}
}

function impressionsChart($chartID) {


	$impressions = get_impressionsChart();

	if(!empty($impressions)) {

		$array = "";
		$count = 0;
		$clicksTotal = 0;
		foreach ($impressions as $date => $click){
			$array .= ",['" . substr($date, 0, 4) . "/" . substr($date, 4, 2) . "/" . substr($date, 6, 2) . "'," . $click . "]";
			$count++;
			$clicksTotal += $click;
		}

		$html = "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(impressionsChart);

      function impressionsChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Impressions']
          " . $array . "
        ]);

        var options = {
          title: 'Impressions - Total: " . round($clicksTotal, 2) . " - Average per day: " . round($clicksTotal / $count, 2). " ',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {title: 'Impressions',minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

		return $html;
	} else {
		return "";
	}
}

function monthlyConversions($chartID) {
	$years = json_decode(file_get_contents("https://superproof.co.uk/wp-content/plugins/web-enquires/cron.php?type=month"));

	$total = 0;
	$array = "";
	foreach ( $years as $year => $months) {
		foreach ($months as $month => $convs){
			$array .= ",['" . $month . "/" . $year . "', " . $convs . "]";
			$total += $convs;
		}
	}

	$html = "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(monthlyconversionsChart);

      function monthlyconversionsChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Enquiries']
          " . $array . "
        ]);

        var options = {
          title: 'Forms filled in per month - Total: " . $total . "',
          curveType: 'function',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'},direction: '-1'},
          vAxis: {title: 'Conversions',minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

	return $html;
}

function allConversionsChart($chartID) {
	$impressions = get_allConversionsChart();

	if ( ! empty( $impressions ) ) {

		$html = "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(allConversionsChart); 

      function allConversionsChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'forms', 'emails', 'phones' ],
          " . $impressions . "
        ]);

        var options = {
          title: 'Conversions by type per month',
          curveType: 'function',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {title: 'Conversions',minValue: 0},
          colors: ['#559bff', '#b87333', '#e55b46']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

		return $html;
	}
}

function allConversionsChartAdwords($chartID) {
	$impressions = get_allConversionsChartAdwords();

	if ( ! empty( $impressions ) ) {

		$html = "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(allConversionsChartAdwords); 

      function allConversionsChartAdwords() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'forms', 'emails', 'phones' ],
          " . $impressions . "
        ]);

        var options = {
          title: 'Conversions by type per month from Adwords',
          curveType: 'function',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {title: 'Conversions',minValue: 0},
          colors: ['#559bff', '#b87333', '#e55b46']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

		return $html;
	}
}

function allConversionsChartOrganic($chartID) {
	$impressions = get_allConversionsChartOrganic();

	if ( ! empty( $impressions ) ) {

		$html = "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(allConversionsChartOrganic); 

      function allConversionsChartOrganic() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'forms', 'emails', 'phones' ],
          " . $impressions . "
        ]);

        var options = {
          title: 'Conversions by type per month from Organic',
          curveType: 'function',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {title: 'Conversions',minValue: 0},
          colors: ['#559bff', '#b87333', '#e55b46']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

		return $html;
	}
}

function conversionRateChart($chartID){
    $costs = get_Percent_conversion_rates();

    if(!empty($costs)) {

        $array = "";
        foreach ($costs as $date => $cost){
            $array .= ",['" . substr($date, 0, 4) . "/" . substr($date, 4, 2) . "/" . substr($date, 6, 2) . "'," . $cost . "]";
        }

        $html = "<script type=\"text/javascript\">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(conversionRateChart);

      function conversionRateChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'conversion rate (%)']
          " . $array . "
        ]);

        var options = {
          title: 'Average Conversion Rate % - Overall " . round(get_Percent_conversion_rates_average(), 2) . "%',
          hAxis: {title: 'date',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0},
          colors: ['#ffc20e']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('" . $chartID . "'));
        chart.draw(data, options);
      }
    </script>";

        return $html;
    } else {
        return "";
    }

}