<?php
/**
 * Created by PhpStorm.
 * User: jake
 * Date: 26/09/2017
 * Time: 10:24
 */
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<?php

$navbar = '
<nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">%s</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="dashboard.php" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-dashboard"></i>
								<p class="hidden-lg hidden-md">Dashboard</p>
                            </a>
                        </li>

                        <li style="padding-top: 15px;position: relative;">
                        <form class="dates-form" action="" method="get" onsubmit="setTimeout(formSubmitted(this), 10000);" >
                                <input type="text" class="dates-dropdown" value="' . get_times() . '">
                        </form>';
                        if (isset($_GET['s'])) {
        $start = date('d/m/Y', $_GET['s']);
    } else {
        $start = date('d/m/Y', strtotime('-30 days'));
    }

    if (isset($_GET['e'])) {
        $end = date('d/m/Y', $_GET['e']);
    } else {
        $end = date('d/m/Y');
    }
                        $navbar .= '<script type="text/javascript">var startdate = "' . $start . '", enddate = "' . $end . '";</script>';
                        
                        $navbar .='</li>
                    </ul>
                    <ul class="nav navbar navbar-right">

                        <li>
                            <a href="logout.php">
                                <p>Log out</p>
                            </a>
                        </li>
						<li class="separator hidden-lg hidden-md"></li>
                    </ul>
                </div>
            </div>
        </nav>';

function nav($title){
    global $navbar;
    print sprintf($navbar, $title);
}

function get_the_nav(){
    global $navbar;
    return $navbar;
}