<?php
/**
 * Created by PhpStorm.
 * User: jake
 * Date: 26/09/2017
 * Time: 10:36
 */
session_start();
define('DebugMode', false);
if( !isset($_SESSION['userid'])) {
	print "<script>window.location = 'index.html'</script>";
}
include_once ('GoogleAnalyticsAPI.class.php');
	include_once ('charts.php');
	include_once ('database.php');
	include_once ('analyticsFunctions.php');
//include_once ('conversion-rate-functions.php');
$db = new database();
$ga = new GoogleAnalyticsAPI('service');
function connect()
{
    global $ga;
	if(!isset($_SESSION['tokenExpires']) || !isset($_SESSION['tokenCreated']) || time() >= ($_SESSION['tokenCreated'] + $_SESSION['tokenExpires'])){
		if(DebugMode == true){
			echo "<pre>";
			var_dump($_SESSION);
			echo "</pre>";
		}
		$ga->auth->setClientId('786826102984-o69344c8iqoph9c2j4a3defh7g3k722r.apps.googleusercontent.com'); // From the APIs console
		$ga->auth->setEmail('786826102984-o69344c8iqoph9c2j4a3defh7g3k722r@developer.gserviceaccount.com'); // From the APIs console
		$ga->auth->setPrivateKey('My Project-67eec1ff0b85.p12'); // Path to the .p12 file

		$auth = $ga->auth->getAccessToken();
// Try to get the AccessToken
		if ($auth['http_code'] == 200) {
			$_SESSION['accessToken'] = $auth['access_token'];
			$_SESSION['tokenExpires'] = $auth['expires_in'];
			$_SESSION['tokenCreated'] = time();
			return true;
		} else {
			return false;
		}
    } else {
        return true;
    }
}

function get_visitors()
{
    global $ga;
    global $db;
    $Cache = file_get_contents("cache/visitors");

    if($Cache != "" && filemtime("cache/visitors") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return json_decode($Cache, true);
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['e'])) {
                if (isset($_GET['s'])) {
                    if ($_GET['s'] > $_GET['e']) {
                        print "	<script type=\"text/javascript\">
                                $(document).ready(function(){ 
                        
                                    demo.initChartist();
                        
                                    $.notify({
                                        icon: 'pe-7s-gift',
                                        message: \"Start Date cannot be before end date!\"
                        
                                    },{
                                        type: 'info', 
                                        timer: 4000
                                    });
                        
                                });
                            </script>";
                        $_GET['s'] = date('Y-m-d', strtotime('-30 days'));
                        $_GET['e'] = date('Y-m-d');
                    } else {
                       /* if (date('Y-m-d', strtotime('-30 days')) > $_GET['e']) {
                            print "	<script type=\"text/javascript\">
                                $(document).ready(function(){
                        
                                    demo.initChartist();
                        
                                    $.notify({
                                        icon: 'pe-7s-gift',
                                        message: \"Start Date cannot be before end date!\"
                        
                                    },{
                                        type: 'info',
                                        timer: 4000
                                    });
                        
                                });
                            </script>";
                            $_GET['s'] = strtotime('-30 days');
                            $_GET['e'] = time();
                        }*/
                    }
                }
            }

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );
            $ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
            $params = array(
                'metrics' => 'ga:visits',
                'dimensions' => 'ga:date',
            );
            $visits = $ga->query($params);
            $return = [];
            foreach ($visits['rows'] as $visit) {
                $date = $visit[0];
                $return[$date] = $visit[1];
            }
            file_put_contents("cache/visitors", json_encode($return));
            return $return;
        } else {
            return false;
        }
    }
}

function get_tech()
{

    global $ga;
    global $db;
    $Cache = file_get_contents("cache/tech");

    if($Cache != "" && filemtime("cache/tech") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return json_decode($Cache, true);
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Id

            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];

            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );
            $ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
            $params = array(
                'metrics' => 'ga:visits',
                'dimensions' => 'ga:browser',
            );
            $browsers = $ga->query($params);
            $return = [];
            foreach ($browsers['rows'] as $browserStat) {
                $browser = $browserStat[0];
                $return[$browser] = $browserStat[1];
            }
            file_put_contents("cache/tech", json_encode($return));
            return $return;
        } else {
            return false;
        }
    }
}

function get_conversion_rates()
{
    global $ga;
    global $db;
    global $db;
    $Cache = file_get_contents("cache/convRates");

    if($Cache != "" && filemtime("cache/convRates") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return json_decode($Cache, true);
    } else {
        print (DebugMode ? "grabbing" : "");

        if (connect()) {

            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d',$_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:adCost',
                'dimensions' => 'ga:date',
            );

            $costs = $ga->query($params);

            $costsArray = [];
            foreach ($costs['rows'] as $cost) {
                $Day = $cost[0];
                $costsArray[$Day] = $cost[1];
            }

            /* CONVERSIONS */

            $params = array(
                'metrics' => 'ga:goal1Completions',
                'dimensions' => 'ga:date',
            );

            $goals = $ga->query($params);
            $goalsArray = [];
            foreach ($goals['rows'] as $goal) {
                $Day = $goal[0];
                $goalsArray[$Day] = $goal[1];
            }

            $cpc = [];
            foreach ($costsArray as $date => $cost) {
                if ($goalsArray[$date] == 0 || $cost == 0) {
                    $cpc[$date] = 0;
                } else {
                    $cpc[$date] = $cost / $goalsArray[$date];
                }
            }
            file_put_contents("cache/convRates", json_encode($cpc));
            return $cpc;
        } else {
            return false;
        }
    }
}

function get_conversion_average(){
    global $ga;
    global $db;
    $Cache = file_get_contents("cache/convAvg");

    if($Cache != "" && filemtime("cache/convAvg") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return $Cache;
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }

            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:adCost',
            );

            $costs = $ga->query($params);

            $overallCost = $costs['totalsForAllResults']['ga:adCost'];

            /* CONVERSIONS */

            $params = array(
                'metrics' => 'ga:goal1Completions'
            );

            $goals = $ga->query($params);

            $conversions = $goals['totalsForAllResults']['ga:goal1Completions'];
            if ($overallCost == 0 || $conversions == 0) {

                file_put_contents("cache/convAvg", 0);

                return 0;
            } else {
                file_put_contents("cache/convAvg", $overallCost / $conversions);
                return $overallCost / $conversions;
            }

        }
    }
}


function get_Percent_conversion_rates()
{
    global $ga;
    global $db;
    $Cache = file_get_contents("cache/percentConvRates");

    if($Cache != "" && filemtime("cache/percentConvRates") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return json_decode($Cache, true);
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Ids
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            /*$params = array(
                'metrics' => 'ga:users',
                'dimensions' => 'ga:date',
            );

            $costs = $ga->query($params);

            $costsArray = [];
            foreach ($costs['rows'] as $cost) {
                $Day = $cost[0];
                $costsArray[$Day] = $cost[1];
            }*/

            /* CONVERSIONS */

            $params = array(
                'metrics' => 'ga:goal1ConversionRate',
                'dimensions' => 'ga:date',
            );

            $goals = $ga->query($params);
            $goalsArray = [];
            foreach ($goals['rows'] as $goal) {
                $Day = $goal[0];
                $goalsArray[$Day] = $goal[1];
            }
            file_put_contents("cache/percentConvRates", json_encode($goalsArray));
            return $goalsArray;
        } else {
            return false;
        }
    }
}

	function get_clicks()
	{
		global $ga;
		global $db;
		//$Cache = file_get_contents("cache/visitors");

		//if($Cache != "" && filemtime("cache/visitors") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
		//	return json_decode($Cache, true);
		//} else {
			print (DebugMode ? "grabbing" : "");
			if (connect()) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
				$ga->setAccountId($id);

				if (isset($_GET['e'])) {
					if (isset($_GET['s'])) {
						if ($_GET['s'] > $_GET['e']) {
							print "	<script type=\"text/javascript\">
                                $(document).ready(function(){ 
                        
                                    demo.initChartist();
                        
                                    $.notify({
                                        icon: 'pe-7s-gift',
                                        message: \"Start Date cannot be before end date!\"
                        
                                    },{
                                        type: 'info', 
                                        timer: 4000
                                    });
                        
                                });
                            </script>";
							$_GET['s'] = date('Y-m-d', strtotime('-30 days'));
							$_GET['e'] = date('Y-m-d');
						} else {
							/* if (date('Y-m-d', strtotime('-30 days')) > $_GET['e']) {
								 print "	<script type=\"text/javascript\">
									 $(document).ready(function(){

										 demo.initChartist();

										 $.notify({
											 icon: 'pe-7s-gift',
											 message: \"Start Date cannot be before end date!\"

										 },{
											 type: 'info',
											 timer: 4000
										 });

									 });
								 </script>";
								 $_GET['s'] = strtotime('-30 days');
								 $_GET['e'] = time();
							 }*/
						}
					}
				}

				if (isset($_GET['s'])) {
					$start = date('Y-m-d', $_GET['s']);
				} else {
					$start = date('Y-m-d', strtotime('-30 days'));
				}

				if (isset($_GET['e'])) {
					$end = date('Y-m-d', $_GET['e']);
				} else {
					$end = date('Y-m-d');
				}


				$defaults = array(
					'start-date' => $start,
					'end-date' => $end,
				);
				$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
				$params = array(
					'metrics' => 'ga:adClicks',
					'dimensions' => 'ga:date',
				);
				$clicks = $ga->query($params);
				$return = [];

				foreach ($clicks['rows'] as $visit) {
					$date = $visit[0];
					$return[$date] = $visit[1];
				}
				//file_put_contents("cache/visitors", json_encode($return));
				return $return;
			} else {
				return false;
			}
		//}
	}

	function get_spend()
	{
		global $ga;
		global $db;
		//$Cache = file_get_contents("cache/visitors");

		//if($Cache != "" && filemtime("cache/visitors") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
		//	return json_decode($Cache, true);
		//} else {
			print (DebugMode ? "grabbing" : "");
			if (connect()) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'] ;
				$ga->setAccountId($id);

				if (isset($_GET['e'])) {
					if (isset($_GET['s'])) {
						if ($_GET['s'] > $_GET['e']) {
							print "	<script type=\"text/javascript\">
                                $(document).ready(function(){ 
                        
                                    demo.initChartist();
                        
                                    $.notify({
                                        icon: 'pe-7s-gift',
                                        message: \"Start Date cannot be before end date!\"
                        
                                    },{
                                        type: 'info', 
                                        timer: 4000
                                    });
                        
                                });
                            </script>";
							$_GET['s'] = date('Y-m-d', strtotime('-30 days'));
							$_GET['e'] = date('Y-m-d');
						} else {
							/* if (date('Y-m-d', strtotime('-30 days')) > $_GET['e']) {
								 print "	<script type=\"text/javascript\">
									 $(document).ready(function(){

										 demo.initChartist();

										 $.notify({
											 icon: 'pe-7s-gift',
											 message: \"Start Date cannot be before end date!\"

										 },{
											 type: 'info',
											 timer: 4000
										 });

									 });
								 </script>";
								 $_GET['s'] = strtotime('-30 days');
								 $_GET['e'] = time();
							 }*/
						}
					}
				}

				if (isset($_GET['s'])) {
					$start = date('Y-m-d', $_GET['s']);
				} else {
					$start = date('Y-m-d', strtotime('-30 days'));
				}

				if (isset($_GET['e'])) {
					$end = date('Y-m-d', $_GET['e']);
				} else {
					$end = date('Y-m-d');
				}


				$defaults = array(
					'start-date' => $start,
					'end-date' => $end,
				);
				$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
				$params = array(
					'metrics' => 'ga:adCost',
					'dimensions' => 'ga:date',
				);
				$clicks = $ga->query($params);
				$return = [];

				foreach ($clicks['rows'] as $visit) {
					$date = $visit[0];
					$return[$date] = $visit[1];
				}
				//file_put_contents("cache/visitors", json_encode($return));
				return $return;
			} else {
				return false;
			}
		//}
	}

	function get_impressionsChart()
	{
		global $ga;
		global $db;
		//$Cache = file_get_contents("cache/visitors");

		//if($Cache != "" && filemtime("cache/visitors") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
		//	return json_decode($Cache, true);
		//} else {
			print (DebugMode ? "grabbing" : "");
			if (connect()) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
				$ga->setAccountId($id);

				if (isset($_GET['e'])) {
					if (isset($_GET['s'])) {
						if ($_GET['s'] > $_GET['e']) {
							print "	<script type=\"text/javascript\">
                                $(document).ready(function(){ 
                        
                                    demo.initChartist();
                        
                                    $.notify({
                                        icon: 'pe-7s-gift',
                                        message: \"Start Date cannot be before end date!\"
                        
                                    },{
                                        type: 'info', 
                                        timer: 4000
                                    });
                        
                                });
                            </script>";
							$_GET['s'] = date('Y-m-d', strtotime('-30 days'));
							$_GET['e'] = date('Y-m-d');
						} else {
							/* if (date('Y-m-d', strtotime('-30 days')) > $_GET['e']) {
								 print "	<script type=\"text/javascript\">
									 $(document).ready(function(){

										 demo.initChartist();

										 $.notify({
											 icon: 'pe-7s-gift',
											 message: \"Start Date cannot be before end date!\"

										 },{
											 type: 'info',
											 timer: 4000
										 });

									 });
								 </script>";
								 $_GET['s'] = strtotime('-30 days');
								 $_GET['e'] = time();
							 }*/
						}
					}
				}

				if (isset($_GET['s'])) {
					$start = date('Y-m-d', $_GET['s']);
				} else {
					$start = date('Y-m-d', strtotime('-30 days'));
				}

				if (isset($_GET['e'])) {
					$end = date('Y-m-d', $_GET['e']);
				} else {
					$end = date('Y-m-d');
				}


				$defaults = array(
					'start-date' => $start,
					'end-date' => $end,
				);
				$ga->setDefaultQueryParams($defaults);

// Example1: Get visits by date
				$params = array(
					'metrics' => 'ga:impressions',
					'dimensions' => 'ga:date',
				);
				$clicks = $ga->query($params);
				$return = [];

				foreach ($clicks['rows'] as $visit) {
					$date = $visit[0];
					$return[$date] = $visit[1];
				}
				//file_put_contents("cache/visitors", json_encode($return));
				return $return;
			} else {
				return false;
			}
		//}
	}

	function get_long_email_clicks()
	{
		global $ga;
		global $db;
		global $db;
		global $requestCache;
		if (connect()) {
			if ($requestCache == 0) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
				$ga->setAccountId($id);

				/*if (isset($_GET['s'])) {
					$start = date('Y-m-d', $_GET['s']);
				} else {
					$start = date('Y-m-d', strtotime('-1 month'));
				}

				if (isset($_GET['e'])) {
					$end = date('Y-m-d', $_GET['e']);
				} else {
					$end = date('Y-m-d');
				}*/

				$start = date('Y-m-d', strtotime('-1 year'));
				$end = date('Y-m-d', strtotime('last day of previous month'));


				$defaults = array(
					'start-date' => $start,
					'end-date' => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams($defaults);

				$params = array(
					'metrics' => 'ga:totalEvents',
					'dimensions' => 'ga:yearmonth, ga:eventLabel'
				);

				$costs = $ga->query($params);
				$array = array();
				foreach ($costs['rows'] as $row){
					if(strpos($row[1], "mailto:") !== false){
						$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
						if(isset($array[$date])){
							$array[$date] += $row[2];
						} else {
							$array[$date] = $row[2];
						}
					}
				}
				if($costs['totalResults'] > 1001){
					$start = date('Y-m-d', strtotime('-1 year'));
					$end = date('Y-m-d', strtotime('last day of previous month'));


					$defaults = array(
						'start-date' => $start,
						'end-date' => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams($defaults);

					$params = array(
						'metrics' => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel',
						'start-index' => 1000
					);

					$costs = $ga->query($params);
					foreach ($costs['rows'] as $row){
						if(strpos($row[1], "mailto:") !== false){
							$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
							if(isset($array[$date])){
								$array[$date] += $row[2];
							} else {
								$array[$date] = $row[2];
							}
						}
					}
				}
				return $array;
			}
		}
	}

	function get_long_email_clicks_adwords()
	{
		global $ga;
		global $db;
		global $db;
		global $requestCache;
		if (connect()) {
			if ($requestCache == 0) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
				$ga->setAccountId($id);

				/*if (isset($_GET['s'])) {
					$start = date('Y-m-d', $_GET['s']);
				} else {
					$start = date('Y-m-d', strtotime('-1 month'));
				}

				if (isset($_GET['e'])) {
					$end = date('Y-m-d', $_GET['e']);
				} else {
					$end = date('Y-m-d');
				}*/

				$start = date('Y-m-d', strtotime('-1 year'));
				$end = date('Y-m-d', strtotime('last day of previous month'));


				$defaults = array(
					'start-date' => $start,
					'end-date' => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams($defaults);

				$params = array(
					'metrics' => 'ga:totalEvents',
					'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium'
				);

				$costs = $ga->query($params);
				$array = array();
				foreach ($costs['rows'] as $row){
					if(strpos($row[1], "mailto:") !== false && $row[2] == "cpc"){
						$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
						if(isset($array[$date])){
							$array[$date] += $row[3];
						} else {
							$array[$date] = $row[3];
						}
					}
				}
				if($costs['totalResults'] > 1001){
					$start = date('Y-m-d', strtotime('-1 year'));
					$end = date('Y-m-d', strtotime('last day of previous month'));


					$defaults = array(
						'start-date' => $start,
						'end-date' => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams($defaults);

					$params = array(
						'metrics' => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium',
						'start-index' => 1000
					);

					$costs = $ga->query($params);
					foreach ($costs['rows'] as $row){
						if(strpos($row[1], "mailto:") !== false && $row[2] == "cpc"){
							$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
							if(isset($array[$date])){
								$array[$date] += $row[3];
							} else {
								$array[$date] = $row[3];
							}
						}
					}
				}
				if($costs['totalResults'] > 2001){
					$start = date('Y-m-d', strtotime('-1 year'));
					$end = date('Y-m-d', strtotime('last day of previous month'));


					$defaults = array(
						'start-date' => $start,
						'end-date' => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams($defaults);

					$params = array(
						'metrics' => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium',
						'start-index' => 2000
					);

					$costs = $ga->query($params);
					foreach ($costs['rows'] as $row){
						if(strpos($row[1], "mailto:") !== false && $row[2] == "cpc"){
							$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
							if(isset($array[$date])){
								$array[$date] += $row[3];
							} else {
								$array[$date] = $row[3];
							}
						}
					}
				}
				return $array;
			}
		}
	}

	function get_long_email_clicks_organic()
	{
		global $ga;
		global $db;
		global $db;
		global $requestCache;
		if (connect()) {
			if ($requestCache == 0) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
				$ga->setAccountId($id);

				/*if (isset($_GET['s'])) {
					$start = date('Y-m-d', $_GET['s']);
				} else {
					$start = date('Y-m-d', strtotime('-1 month'));
				}

				if (isset($_GET['e'])) {
					$end = date('Y-m-d', $_GET['e']);
				} else {
					$end = date('Y-m-d');
				}*/

				$start = date('Y-m-d', strtotime('-1 year'));
				$end = date('Y-m-d', strtotime('last day of previous month'));


				$defaults = array(
					'start-date' => $start,
					'end-date' => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams($defaults);

				$params = array(
					'metrics' => 'ga:totalEvents',
					'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium'
				);

				$costs = $ga->query($params);
				$array = array();
				foreach ($costs['rows'] as $row){
					if(strpos($row[1], "mailto:") !== false && $row[2] == "organic"){
						$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
						if(isset($array[$date])){
							$array[$date] += $row[3];
						} else {
							$array[$date] = $row[3];
						}
					}
				}
				if($costs['totalResults'] > 1001){
					$start = date('Y-m-d', strtotime('-1 year'));
					$end = date('Y-m-d', strtotime('last day of previous month'));


					$defaults = array(
						'start-date' => $start,
						'end-date' => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams($defaults);

					$params = array(
						'metrics' => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium',
						'start-index' => 1000
					);

					$costs = $ga->query($params);
					foreach ($costs['rows'] as $row){
						if(strpos($row[1], "mailto:") !== false && $row[2] == "organic"){
							$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
							if(isset($array[$date])){
								$array[$date] += $row[3];
							} else {
								$array[$date] = $row[3];
							}
						}
					}
				}
				if($costs['totalResults'] > 2001){
					$start = date('Y-m-d', strtotime('-1 year'));
					$end = date('Y-m-d', strtotime('last day of previous month'));


					$defaults = array(
						'start-date' => $start,
						'end-date' => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams($defaults);

					$params = array(
						'metrics' => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium',
						'start-index' => 2000
					);

					$costs = $ga->query($params);
					foreach ($costs['rows'] as $row){
						if(strpos($row[1], "mailto:") !== false && $row[2] == "organic"){
							$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
							if(isset($array[$date])){
								$array[$date] += $row[3];
							} else {
								$array[$date] = $row[3];
							}
						}
					}
				}
				return $array;
			}
		}
	}

function get_long_phone_clicks_adwords()
	{
		global $ga;
		global $db;
		global $db;
		global $requestCache;
		if (connect()) {
			if ($requestCache == 0) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
				$ga->setAccountId($id);

				/*if (isset($_GET['s'])) {
					$start = date('Y-m-d', $_GET['s']);
				} else {
					$start = date('Y-m-d', strtotime('-1 month'));
				}

				if (isset($_GET['e'])) {
					$end = date('Y-m-d', $_GET['e']);
				} else {
					$end = date('Y-m-d');
				}*/

				$start = date('Y-m-d', strtotime('-1 year'));
				$end = date('Y-m-d', strtotime('last day of previous month'));


				$defaults = array(
					'start-date' => $start,
					'end-date' => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams($defaults);

				$params = array(
					'metrics' => 'ga:totalEvents',
					'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium'
				);

				$costs = $ga->query($params);
				$array = array();
				foreach ($costs['rows'] as $row){
					if(strpos($row[1], "tel:") !== false && $row[2] == "cpc"){
						$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
						if(isset($array[$date])){
							$array[$date] += $row[3];
						} else {
							$array[$date] = $row[3];
						}
					}
				}
				if($costs['totalResults'] > 1001){
					$start = date('Y-m-d', strtotime('-1 year'));
					$end = date('Y-m-d', strtotime('last day of previous month'));


					$defaults = array(
						'start-date' => $start,
						'end-date' => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams($defaults);

					$params = array(
						'metrics' => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium',
						'start-index' => 1000
					);

					$costs = $ga->query($params);
					foreach ($costs['rows'] as $row){
						if(strpos($row[1], "tel:") !== false && $row[2] == "cpc"){
							$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
							if(isset($array[$date])){
								$array[$date] += $row[3];
							} else {
								$array[$date] = $row[3];
							}
						}
					}
				}
				if($costs['totalResults'] > 2001){
					$start = date('Y-m-d', strtotime('-1 year'));
					$end = date('Y-m-d', strtotime('last day of previous month'));


					$defaults = array(
						'start-date' => $start,
						'end-date' => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams($defaults);

					$params = array(
						'metrics' => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium',
						'start-index' => 2000
					);

					$costs = $ga->query($params);
					foreach ($costs['rows'] as $row){
						if(strpos($row[1], "tel:") !== false && $row[2] == "cpc"){
							$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
							if(isset($array[$date])){
								$array[$date] += $row[3];
							} else {
								$array[$date] = $row[3];
							}
						}
					}
				}
				return $array;
			}
		}
	}

function get_long_phone_clicks_organic()
	{
		global $ga;
		global $db;
		global $db;
		global $requestCache;
		if (connect()) {
			if ($requestCache == 0) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
				$ga->setAccountId($id);

				/*if (isset($_GET['s'])) {
					$start = date('Y-m-d', $_GET['s']);
				} else {
					$start = date('Y-m-d', strtotime('-1 month'));
				}

				if (isset($_GET['e'])) {
					$end = date('Y-m-d', $_GET['e']);
				} else {
					$end = date('Y-m-d');
				}*/

				$start = date('Y-m-d', strtotime('-1 year'));
				$end = date('Y-m-d', strtotime('last day of previous month'));


				$defaults = array(
					'start-date' => $start,
					'end-date' => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams($defaults);

				$params = array(
					'metrics' => 'ga:totalEvents',
					'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium'
				);

				$costs = $ga->query($params);
				$array = array();
				foreach ($costs['rows'] as $row){
					if(strpos($row[1], "tel:") !== false && $row[2] == "organic"){
						$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
						if(isset($array[$date])){
							$array[$date] += $row[3];
						} else {
							$array[$date] = $row[3];
						}
					}
				}
				if($costs['totalResults'] > 1001){
					$start = date('Y-m-d', strtotime('-1 year'));
					$end = date('Y-m-d', strtotime('last day of previous month'));


					$defaults = array(
						'start-date' => $start,
						'end-date' => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams($defaults);

					$params = array(
						'metrics' => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium',
						'start-index' => 1000
					);

					$costs = $ga->query($params);
					foreach ($costs['rows'] as $row){
						if(strpos($row[1], "tel:") !== false && $row[2] == "organic"){
							$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
							if(isset($array[$date])){
								$array[$date] += $row[3];
							} else {
								$array[$date] = $row[3];
							}
						}
					}
				}
				if($costs['totalResults'] > 2001){
					$start = date('Y-m-d', strtotime('-1 year'));
					$end = date('Y-m-d', strtotime('last day of previous month'));


					$defaults = array(
						'start-date' => $start,
						'end-date' => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams($defaults);

					$params = array(
						'metrics' => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium',
						'start-index' => 2000
					);

					$costs = $ga->query($params);
					foreach ($costs['rows'] as $row){
						if(strpos($row[1], "tel:") !== false && $row[2] == "organic"){
							$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
							if(isset($array[$date])){
								$array[$date] += $row[3];
							} else {
								$array[$date] = $row[3];
							}
						}
					}
				}
				return $array;
			}
		}
	}

function get_long_conversions_adwords()
	{
		global $ga;
		global $db;
		global $db;
		global $requestCache;
		if (connect()) {
			if ($requestCache == 0) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
				$ga->setAccountId($id);

				/*if (isset($_GET['s'])) {
					$start = date('Y-m-d', $_GET['s']);
				} else {
					$start = date('Y-m-d', strtotime('-1 month'));
				}

				if (isset($_GET['e'])) {
					$end = date('Y-m-d', $_GET['e']);
				} else {
					$end = date('Y-m-d');
				}*/

				$start = date('Y-m-d', strtotime('-1 year'));
				$end = date('Y-m-d', strtotime('last day of previous month'));


				$defaults = array(
					'start-date' => $start,
					'end-date' => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams($defaults);

				$params = array(
					'metrics' => 'ga:goal1Completions',
					'dimensions' => 'ga:yearmonth, ga:medium'
				);

				$costs = $ga->query($params);
				$array = array();
				foreach ($costs['rows'] as $row){
					if($row[1] == "cpc"){
						$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
						if(isset($array[$date])){
							$array[$date] += $row[2];
						} else {
							$array[$date] = $row[2];
						}
					}
				}

				if($costs['totalResults'] > 1001){
					$start = date('Y-m-d', strtotime('-1 year'));
					$end = date('Y-m-d', strtotime('last day of previous month'));


					$defaults = array(
						'start-date' => $start,
						'end-date' => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams($defaults);

					$params = array(
						'metrics' => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium',
						'start-index' => 1000
					);

					$costs = $ga->query($params);
					foreach ($costs['rows'] as $row){
						if($row[1] == "cpc"){
							$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
							if(isset($array[$date])){
								$array[$date] += $row[2];
							} else {
								$array[$date] = $row[2];
							}
						}
					}
				}
				if($costs['totalResults'] > 2001){
					$start = date('Y-m-d', strtotime('-1 year'));
					$end = date('Y-m-d', strtotime('last day of previous month'));


					$defaults = array(
						'start-date' => $start,
						'end-date' => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams($defaults);

					$params = array(
						'metrics' => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium',
						'start-index' => 2000
					);

					$costs = $ga->query($params);
					foreach ($costs['rows'] as $row){
						if($row[1] == "cpc"){
							$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
							if(isset($array[$date])){
								$array[$date] += $row[2];
							} else {
								$array[$date] = $row[2];
							}
						}
					}
				}
				return $array;
			}
		}
	}

	function get_long_conversions_organic()
	{
		global $ga;
		global $db;
		global $db;
		global $requestCache;
		if (connect()) {
			if ($requestCache == 0) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
				$ga->setAccountId($id);

				/*if (isset($_GET['s'])) {
					$start = date('Y-m-d', $_GET['s']);
				} else {
					$start = date('Y-m-d', strtotime('-1 month'));
				}

				if (isset($_GET['e'])) {
					$end = date('Y-m-d', $_GET['e']);
				} else {
					$end = date('Y-m-d');
				}*/

				$start = date('Y-m-d', strtotime('-1 year'));
				$end = date('Y-m-d', strtotime('last day of previous month'));


				$defaults = array(
					'start-date' => $start,
					'end-date' => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams($defaults);

				$params = array(
					'metrics' => 'ga:goal1Completions',
					'dimensions' => 'ga:yearmonth, ga:medium'
				);

				$costs = $ga->query($params);
				$array = array();
				foreach ($costs['rows'] as $row){
					if($row[1] == "organic"){
						$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
						if(isset($array[$date])){
							$array[$date] += $row[2];
						} else {
							$array[$date] = $row[2];
						}
					}
				}

				if($costs['totalResults'] > 1001){
					$start = date('Y-m-d', strtotime('-1 year'));
					$end = date('Y-m-d', strtotime('last day of previous month'));


					$defaults = array(
						'start-date' => $start,
						'end-date' => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams($defaults);

					$params = array(
						'metrics' => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium',
						'start-index' => 1000
					);

					$costs = $ga->query($params);
					foreach ($costs['rows'] as $row){
						if($row[1] == "organic"){
							$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
							if(isset($array[$date])){
								$array[$date] += $row[2];
							} else {
								$array[$date] = $row[2];
							}
						}
					}
				}
				if($costs['totalResults'] > 2001){
					$start = date('Y-m-d', strtotime('-1 year'));
					$end = date('Y-m-d', strtotime('last day of previous month'));


					$defaults = array(
						'start-date' => $start,
						'end-date' => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams($defaults);

					$params = array(
						'metrics' => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel, ga:medium',
						'start-index' => 2000
					);

					$costs = $ga->query($params);
					foreach ($costs['rows'] as $row){
						if($row[1] == "organic"){
							$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
							if(isset($array[$date])){
								$array[$date] += $row[2];
							} else {
								$array[$date] = $row[2];
							}
						}
					}
				}
				return $array;
			}
		}
	}


	function get_long_phone_clicks()
	{
		global $ga;
		global $db;
		global $db;
		global $requestCache;
		if (connect()) {
			if ($requestCache == 0) {
				// Set the accessToken and Account-Id
				$ga->setAccessToken($_SESSION['accessToken']);
				$id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
				$ga->setAccountId($id);

				/*if (isset($_GET['s'])) {
					$start = date('Y-m-d', $_GET['s']);
				} else {
					$start = date('Y-m-d', strtotime('-1 month'));
				}

				if (isset($_GET['e'])) {
					$end = date('Y-m-d', $_GET['e']);
				} else {
					$end = date('Y-m-d');
				}*/

				$start = date('Y-m-d', strtotime('-1 year'));
				$end = date('Y-m-d', strtotime('last day of previous month'));


				$defaults = array(
					'start-date' => $start,
					'end-date' => $end,
				);

				/* COSTS */

				$ga->setDefaultQueryParams($defaults);

				$params = array(
					'metrics' => 'ga:totalEvents',
					'dimensions' => 'ga:yearmonth, ga:eventLabel'
				);

				$costs = $ga->query($params);
				$array = array();
				foreach ($costs['rows'] as $row){
					if(strpos($row[1], "tel:") !== false){
						$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
						if(isset($array[$date])){
							$array[$date] += $row[2];
						} else {
							$array[$date] = $row[2];
						}
					}
				}
				if($costs['totalResults'] > 1001){
					$start = date('Y-m-d', strtotime('-1 year'));
					$end = date('Y-m-d', strtotime('last day of previous month'));


					$defaults = array(
						'start-date' => $start,
						'end-date' => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams($defaults);

					$params = array(
						'metrics' => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel',
						'start-index' => 1000
					);

					$costs = $ga->query($params);
					foreach ($costs['rows'] as $row){
						if(strpos($row[1], "tel:") !== false){
							$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
							if(isset($array[$date])){
								$array[$date] += $row[2];
							} else {
								$array[$date] = $row[2];
							}
						}
					}
				}
				if($costs['totalResults'] > 2001){
					$start = date('Y-m-d', strtotime('-1 year'));
					$end = date('Y-m-d', strtotime('last day of previous month'));


					$defaults = array(
						'start-date' => $start,
						'end-date' => $end,
					);

					/* COSTS */

					$ga->setDefaultQueryParams($defaults);

					$params = array(
						'metrics' => 'ga:totalEvents',
						'dimensions' => 'ga:yearmonth, ga:eventLabel',
						'start-index' => 2000
					);

					$costs = $ga->query($params);
					foreach ($costs['rows'] as $row){
						if(strpos($row[1], "tel:") !== false){
							$date = substr($row[0], 0, 4 ) . substr($row[0], 4, 2);
							if(isset($array[$date])){
								$array[$date] += $row[2];
							} else {
								$array[$date] = $row[2];
							}
						}
					}
				}
				return $array;
			}
		}
	}

	function get_allConversionsChart()
	{
		$emails = get_long_email_clicks();
		$phone = get_long_phone_clicks();
		$forms = get_long_conversions();

		$array = "";
		foreach ($emails as $date => $emailClicks) {
			$thedate = substr($date, 0, 4) . "/" . substr($date, 4, 2);
			$phoneClicks = $phone[$date];
			$formsCount = $forms[$date];
			if($array == ""){
				$array .= "['" . $thedate . "', " . $formsCount . ", " . $emailClicks . ", " . $phoneClicks . "]";
			} else {
				$array .= ",['" . $thedate . "', " . $formsCount . ", " . $emailClicks . ", " . $phoneClicks . "]";
			}
		}

		return $array;

	}


	function get_allConversionsChartAdwords()
	{
		$emails = get_long_email_clicks_adwords();
		$phone = get_long_phone_clicks_adwords();
		$forms = get_long_conversions_adwords();

		$array = "";
		foreach ($emails as $date => $emailClicks) {
			$thedate = substr($date, 0, 4) . "/" . substr($date, 4, 2);
			$phoneClicks = $phone[$date];
			$formsCount = $forms[$date];
			if($array == ""){
				$array .= "['" . $thedate . "', " . $formsCount . ", " . $emailClicks . ", " . $phoneClicks . "]";
			} else {
				$array .= ",['" . $thedate . "', " . $formsCount . ", " . $emailClicks . ", " . $phoneClicks . "]";
			}
		}

		return $array;

	}

	function get_allConversionsChartOrganic()
	{
		$emails = get_long_email_clicks_organic();
		$phone = get_long_phone_clicks_organic();
		$forms = get_long_conversions_organic();

		$array = "";
		foreach ($emails as $date => $emailClicks) {
			$thedate = substr($date, 0, 4) . "/" . substr($date, 4, 2);
			$phoneClicks = $phone[$date];
			$formsCount = $forms[$date];
			if($array == ""){
				$array .= "['" . $thedate . "', " . $formsCount . ", " . $emailClicks . ", " . $phoneClicks . "]";
			} else {
				$array .= ",['" . $thedate . "', " . $formsCount . ", " . $emailClicks . ", " . $phoneClicks . "]";
			}
		}

		return $array;

	}


	function get_long_conversions() {
		$conversions = json_decode(file_get_contents("https://superproof.co.uk/wp-content/plugins/web-enquires/cron.php?type=month&count=1000"), true);
		$array = array();
		foreach ($conversions as $year => $months){
			foreach ($months as $month => $count){
				$date = "20" . $year . $month;
				$array[$date] = $count;
			}
		}

		return $array;
	}

	function get_conversions() {
		$conversions = json_decode(file_get_contents("https://superproof.co.uk/wp-content/plugins/web-enquires/cron.php?type=json&count=1000"), true);


		return $conversions;
	}

function get_Percent_conversion_rates_average()
{
    global $ga;
    global $db;
    $Cache = file_get_contents("cache/percentConvRatesavg");

    if ($Cache != "" && filemtime("cache/percentConvRatesavg") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))) {
        return $Cache;
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-30 days'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            $ga->setDefaultQueryParams($defaults);


            /* CONVERSIONS */

            $params = array(
                'metrics' => 'ga:goal1ConversionRate'
            );

            $goals = $ga->query($params);
            $conversions = $goals['totalsForAllResults']['ga:goal1ConversionRate'];
            file_put_contents("cache/percentConvRatesavg", $conversions);
            return $conversions;

        }
    }
}

function get_keywords() {
    global $ga;
    global $db;

    $Cache = file_get_contents("cache/keywords");

    if($Cache != "" && filemtime("cache/keywords") > strtotime("-30 minutes", time()) && (!isset($_GET['s']) || !isset($_GET['e']))){
        return json_decode($Cache, true);
    } else {
        print (DebugMode ? "grabbing" : "");
        if (connect()) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                //$start = date('Y-m-d', strtotime('-30 days'));
                $start = date('Y-m-d', strtotime('29th november 2017'));
                //var_dump($start);
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            $ga->setDefaultQueryParams($defaults);


            /* CONVERSIONS */

            $params = array(
                'metrics' => 'ga:adCost,ga:goal1Completions,ga:sessions',
                'dimensions' => 'ga:keyword'

            );
            $keywords = array();
            $keywordCost = $ga->query($params);

            foreach ($keywordCost['rows'] as $row) {
                $keyword[$row[0]]["cost"] = $row[1];
                $keyword[$row[0]]["conversions"] = $row[2];
                $keyword[$row[0]]["users"] = $row[3];
                if ($keyword[$row[0]]["conversions"] != 0 && $keyword[$row[0]]["users"] != 0) {
                    $keyword[$row[0]]["conversionRate"] = $keyword[$row[0]]["conversions"] / $keyword[$row[0]]["users"];
                } else {
                    $keyword[$row[0]]["conversionRate"] = 0;
                }

                if ($keyword[$row[0]]["cost"] != 0 && $keyword[$row[0]]["conversions"] != 0) {
                    $keyword[$row[0]]["costPerConversion"] = $keyword[$row[0]]["cost"] / $keyword[$row[0]]["conversions"];
                } else {
                    $keyword[$row[0]]["costPerConversion"] = $keyword[$row[0]]["cost"];
                }
            }
            file_put_contents("cache/keywords", json_encode($keyword));
            return $keyword;

        } else {
            return false;
        }
    }
}
