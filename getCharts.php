<?php
	/**
	 * Created by PhpStorm.
	 * User: jake
	 * Date: 04/04/2018
	 * Time: 11:50
	 */
	include_once ('analyticsFunctions.php');
	include_once ('charts.php');


	if(isset($_GET['chart'])){
		switch ($_GET['chart']):

			case "tech":
			        print "<div id=\"TechChart\" style=\"height: 477px;\"></div>" . techChart('TechChart');

			case "traffic":
					print "<div id=\"TrafficChart\" style=\"height: 477px;\"></div>" . trafficChart('TrafficChart');

			case "costPerConversionChart":
					print "<div id=\"costPerConversionChart\" style=\"height: 477px;\"></div>" . costPerConversionChart('costPerConversionChart');

        endswitch;
	}