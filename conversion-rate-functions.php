<?php
/**
 * Created by PhpStorm.
 * User: jake
 * Date: 17/10/2017
 * Time: 13:59
 */

include_once ('GoogleAnalyticsAPI.class.php');
$db = new database();
$ga = new GoogleAnalyticsAPI('service');
$requestCache = 0;
function get_adwords_cost()
{
    global $ga;
    global $db;
    global $db;
    global $requestCache;
    if (connect()) {
        if ($requestCache == 0) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-1 month'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:adCost',
            );

            $costs = $ga->query($params);
            return $costs['totalsForAllResults']['ga:adCost'];
        }
    }
}

function get_adwords_clicks()
{
    global $ga;
    global $db;
    global $db;
    global $requestCache;
    if (connect()) {
        if ($requestCache == 0) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-1 month'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:adClicks',
            );

            $costs = $ga->query($params);
            return $costs['totalsForAllResults']['ga:adClicks'];
        }
    }
}

function get_GAQ_page_visits()
{
    global $ga;
    global $db;
    global $db;
    global $requestCache;
    if (connect()) {
        if ($requestCache == 0) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-1 month'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:goal2Completions',
                'dimensions' => 'ga:source'
            );

            $costs = $ga->query($params);
            $clicks = 0;
            foreach ($costs['rows'] as $row){
                if($row[0] == "google"){
                    $clicks += $row[1];
                }
            }
            return $clicks;
        }
    }
}


function get_form_submissions()
{
    global $ga;
    global $db;
    global $db;
    global $requestCache;
    if (connect()) {
        if ($requestCache == 0) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-1 month'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:goal1Completions',
                'dimensions' => 'ga:source'
            );

            $costs = $ga->query($params);
            $clicks = 0;
            foreach ($costs['rows'] as $row){
                if($row[0] == "google"){
                    $clicks += $row[1];
                }
            }
            return $clicks;
        }
    }
}

function get_email_clicks()
{
    global $ga;
    global $db;
    global $db;
    global $requestCache;
    if (connect()) {
        if ($requestCache == 0) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-1 month'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:totalEvents',
                'dimensions' => 'ga:eventLabel, ga:source'
            );

            $costs = $ga->query($params);
            $clicks = 0;
            foreach ($costs['rows'] as $row){
                if(strpos($row[0], "mailto:") !== false && $row[1] == "google"){
                    $clicks += $row[2];
                }
            }
            return $clicks;
        }
    }
}

function get_phone_clicks()
{
    global $ga;
    global $db;
    global $db;
    global $requestCache;
    if (connect()) {
        if ($requestCache == 0) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-1 month'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:totalEvents',
                'dimensions' => 'ga:eventLabel, ga:source'
            );

            $costs = $ga->query($params);
            $clicks = 0;
            foreach ($costs['rows'] as $row){
                if(strpos($row[0], "tel:") !== false && $row[1] == "google"){
                    $clicks += $row[2];
                }
            }

            return $clicks;
        }
    }
}

function get_fb_clicks()
{
    global $ga;
    global $db;
    global $db;
    global $requestCache;
    if (connect()) {
        if ($requestCache == 0) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-1 month'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:campaign',
                'dimensions' => 'ga:source'
            );

            $costs = $ga->query($params);
            $clicks = 0;
            foreach ($costs['rows'] as $row){
                if(strpos($row[0], "Facebook_London") !== false ||strpos($row[0], "Facebook_Birmingham") !== false){
                    $clicks += $row[1];
                }
            }
            return $clicks;
        }
    }
}

function get_fb_GAQ_page_visits()
{
    global $ga;
    global $db;
    global $db;
    global $requestCache;
    if (connect()) {
        if ($requestCache == 0) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-1 month'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:goal2Completions',
                'dimensions' => 'ga:source'
            );

            $costs = $ga->query($params);
            $clicks = 0;

            foreach ($costs['rows'] as $row){
                if(strpos($row[0], "facebook") !== false || strpos($row[0], "(direct)") !== false){
                    $clicks += $row[1];
                }
            }
            return $clicks;
        }
    }
}

function get_fb_form_submissions()
{
    global $ga;
    global $db;
    global $db;
    global $requestCache;
    if (connect()) {
        if ($requestCache == 0) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-1 month'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:goal1Completions',
                'dimensions' => 'ga:source'
            );

            $costs = $ga->query($params);
            $clicks = 0;
            foreach ($costs['rows'] as $row){
                if(strpos($row[0], "facebook") !== false || strpos($row[0], "(direct)") !== false){
                    $clicks += $row[1];
                }
            }
            return $clicks;
        }
    }
}

function get_fb_email_clicks()
{
    global $ga;
    global $db;
    global $db;
    global $requestCache;
    if (connect()) {
        if ($requestCache == 0) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-1 month'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:totalEvents',
                'dimensions' => 'ga:eventLabel, ga:source'
            );

            $costs = $ga->query($params);
            $clicks = 0;
            foreach ($costs['rows'] as $row){
                    if(strpos($row[0], "mailto:") !== false && (strpos($row[1], "facebook") !== false || strpos($row[1], "(direct)") !== false)){
                    $clicks += $row[2];
                }
            }
            return $clicks;
        }
    }
}

function get_fb_phone_clicks()
{
    global $ga;
    global $db;
    global $db;
    global $requestCache;
    if (connect()) {
        if ($requestCache == 0) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-1 month'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:totalEvents',
                'dimensions' => 'ga:eventLabel,ga:source'
            );

            $costs = $ga->query($params);
            $clicks = 0;

            foreach ($costs['rows'] as $row){
                if(strpos($row[0], "tel:") !== false && (strpos($row[1], "facebook") !== false || strpos($row[1], "direct") !== false)){
                    $clicks += $row[2];
                }
            }
            return $clicks;
        }
    }
}

function get_total_clicks()
{
    global $ga;
    global $db;
    global $db;
    global $requestCache;
    if (connect()) {
        if ($requestCache == 0) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-1 month'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:sessions',
            );

            $costs = $ga->query($params);
            return $costs['totalsForAllResults']['ga:sessions'];
        }
    }
}

function get_total_GAQ_page_visits()
{
    global $ga;
    global $db;
    global $db;
    global $requestCache;
    if (connect()) {
        if ($requestCache == 0) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-1 month'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:goal2Completions'
            );

            $costs = $ga->query($params);
            return $costs['totalsForAllResults']['ga:goal2Completions'];
        }
    }
}

function get_total_form_submissions()
{
    global $ga;
    global $db;
    global $db;
    global $requestCache;
    if (connect()) {
        if ($requestCache == 0) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-1 month'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:goal1Completions'
            );

            $costs = $ga->query($params);
            return $costs['totalsForAllResults']['ga:goal1Completions'];
        }
    }
}

function get_total_email_clicks()
{
    global $ga;
    global $db;
    global $db;
    global $requestCache;
    if (connect()) {
        if ($requestCache == 0) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-1 month'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:totalEvents',
                'dimensions' => 'ga:eventLabel'
            );

            $costs = $ga->query($params);
            $clicks = 0;
            foreach ($costs['rows'] as $row){
                if(strpos($row[0], "mailto:") !== false){
                    $clicks += $row[1];
                }
            }
            return $clicks;
        }
    }
}

function get_total_phone_clicks()
{
    global $ga;
    global $db;
    global $db;
    global $requestCache;
    if (connect()) {
        if ($requestCache == 0) {
            // Set the accessToken and Account-Id
            $ga->setAccessToken($_SESSION['accessToken']);
            $id = $db->query("SELECT propertyId FROM users WHERE id=" . $_SESSION['userid'])[0]['propertyId'];
            $ga->setAccountId($id);

            if (isset($_GET['s'])) {
                $start = date('Y-m-d', $_GET['s']);
            } else {
                $start = date('Y-m-d', strtotime('-1 month'));
            }

            if (isset($_GET['e'])) {
                $end = date('Y-m-d', $_GET['e']);
            } else {
                $end = date('Y-m-d');
            }


            $defaults = array(
                'start-date' => $start,
                'end-date' => $end,
            );

            /* COSTS */

            $ga->setDefaultQueryParams($defaults);

            $params = array(
                'metrics' => 'ga:totalEvents',
                'dimensions' => 'ga:eventLabel'
            );

            $costs = $ga->query($params);
            $clicks = 0;

            foreach ($costs['rows'] as $row){
                if(strpos($row[0], "tel:") !== false ){
                    $clicks += $row[1];
                }
            }
            return $clicks;
        }
    }
}
