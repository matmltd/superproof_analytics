<?php
/**
 * Created by PhpStorm.
 * User: jake
 * Date: 26/09/2017
 * Time: 10:15
 */
session_start();
include_once ('sidebar.php');
include_once ('navigation.php');
include_once ('footer.php');
include_once ('charts.php');
include_once ('database.php');
include_once ('analyticsFunctions.php');
define('CLIENT_ID', 1);

$db = new database();
$siteurl = "http://analytics.superproof.co.uk";
//auth();

function auth(){
    global $db;
    if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true){
        $result = $db->get_token($_SESSION['userid']);
        if(isset($result[0]) && isset($result[0]['token'])){
            if($_SESSION['token'] === $result[0]['token']){
                return true;
            } else {
                header('Location: index.php');
            }
        } else {
            header('Location: index.php');
        }
    } else {
        header('Location: index.php');
    }
}

function custom_headers() {
    $headers="";


    $headers .= "<script type=\"text/javascript\" src=\"https://www.gstatic.com/charts/loader.js\"></script>";
    $headers .= '<!-- Include Required Prerequisites -->
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />';
    return $headers;
}

function get_times() {
    if (isset($_GET['s'])) {
        $start = date('d/m/Y', $_GET['s']);
    } else {
        $start = date('d/m/Y', strtotime('-30 days'));
    }

    if (isset($_GET['e'])) {
        $end = date('d/m/Y', $_GET['e']);
    } else {
        $end = date('d/m/Y');
    }
    return $start . " - " .  $end;
}