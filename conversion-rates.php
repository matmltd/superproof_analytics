<?php
include_once ('functions.php');
include_once ('conversion-rate-functions.php');
include_once ('facebook_ads.php');
include "simple_html_dom.php";

?>

<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Light Bootstrap Dashboard by Creative Tim</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>

    <?=custom_headers(); ?>

</head>
<body>

<?php sidebar();?>

<div class="main-panel">
    <?php nav("Conversion Rates"); ?>


    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card" style="padding: 10px">
                        <style>
                            table.tableizer-table, tr, td, th {
                                border: 1px solid;
                                padding: 8px;
                                text-align: center;
                                vertical-align: middle;
                            }
                        </style>
                        <h2 style="text-align: center">Conversion Rates (<?=date('d/m/Y', strtotime('-1 month'))?> - <?=date('d/m/Y')?>)</h2>
                        <?php
                        $table = '<table class="tableizer-table">
                            <thead>
                            <tr class="tableizer-firstrow">
                                <th>Campaign</th>
                                <th>Spend </th>
                                <th>No of advert clicks</th>
                                <th>Get a Quote visits</th>
                                <th>Av cost per GaQ VISTIS</th>
                                <th class="always-hover">GaQ forms submitted</th>
                                <th class="always-hover">GaQ conversion rate</th>
                                <th class="always-hover">Av cost per GaQ submission</th>
								<th class="always-hover important">Avg cost as proporion of total spend for forms</th>
                                <th class="always-hover">Proportion of budget for GaQ</th>
                                <th>Email link clicks</th>
                                <th>Avg cost as proportion of total spend for emails</th>
                                <th>Proportion of budget on Emails</th>
                                <th>Phone Clicks</th>
                                <th>Avg cost as proportion of total spend for phones</th>
                                <th>Proportion of budget on Phones</th>
                                <th>Av cost per interaction (email, phone or form submission)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>';
                        $adwordsCost = get_adwords_cost();
                        $adwordsClicks = get_adwords_clicks();
                        $GAQ_visits = get_GAQ_page_visits();
                        $formSubmissions = get_form_submissions();
                        $emailClicks = get_email_clicks();
                        $phoneClicks = get_phone_clicks();
                        $totalAdwordsInteractions = $formSubmissions+$emailClicks+$phoneClicks;
                        $adwordsformProp = ($formSubmissions / $totalAdwordsInteractions) * $adwordsCost;
                        $adwordsemailProp = ($emailClicks / $totalAdwordsInteractions) * $adwordsCost;
                        $adwordsphoneProp = ($phoneClicks / $totalAdwordsInteractions) * $adwordsCost;
                        $table .= '
                                <!-- Campaign -->
                                <td class="first-col">Adwords</td>
                                <!-- Spend -->
                                <td>£'.round($adwordsCost, 2).'</td>
                                <!-- clicks -->
                                <td>'.$adwordsClicks.'</td>
                                <!-- GaQ visits -->
                                <td>'.$GAQ_visits.'</td>
                                <!-- % of clicks -->
                                <!-- cost per GaQ Conversion  -->
                                <td>£'.round(($adwordsCost / $GAQ_visits), 2).'</td>
                                <!-- GaQ form submissions -->
                                <td class="always-hover">'.$formSubmissions.'</td>
                                <!-- GaQ Conversion rate -->
                                <td class="always-hover">'.round(($formSubmissions / $adwordsClicks) * 100, 2).'%</td>
                                <!-- avg cost per conv -->
                                <td class="always-hover">£'.round(($adwordsCost / $formSubmissions), 2).'</td>
                                <!-- Avg cost as proporion of total spend -->
                                <td class="always-hover important">£'.round($adwordsformProp/$formSubmissions,2).'</td>
                                <!-- Form PROP -->
                                <td class="always-hover">£'.round($adwordsformProp,2).'</td>
                                <!-- email Clicks -->
                                <td>'.$emailClicks.'</td>
                                <!-- Prop of spend on emails -->
                                <td>£' . round($adwordsemailProp/$emailClicks, 2) .'</td>
                                <!-- Email Proportion -->
                                <td>£'.round($adwordsemailProp, 2).'</td>
                                <!-- Phone Clicks -->
                                <td>'.$phoneClicks.'</td>
                                <!-- Prop of spend on phones -->
                                <td>£' . round($adwordsphoneProp/$phoneClicks, 2) .'</td>
                                <!-- phone prop -->
                                <td>'.round($adwordsphoneProp, 2).'</td>
                                <!-- cost per conv -->
                                <td>£'.round($adwordsCost / ($phoneClicks + $emailClicks + $formSubmissions), 2).'</td>
                            </tr>
                            <tr>';

                        /*$fields = array('clicks', 'spend'); // The fields I ask for?
                        $params = array(
                            'data_columns' => array(
                                'clicks',
                                'spend'
                            ),
                            'date_preset' => 'last_month',
                        );

                        $fbstats = get_fb_stats($fields, $params);
                        $FBtotal = $fbstats['spend'];
                        $fbclicks = $fbstats['clicks'];
                        /*$FBtotal = 250;
                        $fbPhoneClicks = get_fb_phone_clicks();
                        /*$fbclicks = get_fb_clicks();
                        $fbGAQvisits = get_fb_GAQ_page_visits();
                        $fbFormSubmissions = get_fb_form_submissions();
                        $fbEmailClicks = get_fb_email_clicks();
                        $totalFBInteractions = $fbFormSubmissions+$fbEmailClicks+$fbPhoneClicks;
                        $fbFormProp = ($fbFormSubmissions / $totalFBInteractions) * $FBtotal;
                        $fbEmailProp = ($fbEmailClicks / $totalFBInteractions) * $FBtotal;
                        $fbPhoneProp = ($fbPhoneClicks / $totalFBInteractions) * $FBtotal;



                        $table .='
                                <!-- Campaign -->
                                <td class="first-col">Facebook</td>
                                <!-- Spend -->
                                <td>£'.round($FBtotal, 2).'</td>
                                <!-- clicks -->
                                <td>'.$fbclicks.'</td>
                                <!-- GaQ visits -->
                                <td>'.$fbGAQvisits.'</td>
                                <!-- % of clicks -->
                                <!-- cost per GaQ Conversion  -->
                                <td>£'.round(($FBtotal / $fbGAQvisits), 2).'</td>
                                <!-- GaQ form submissions -->
                                <td  class="always-hover">'.$fbFormSubmissions.'</td>
                                <!-- GaQ Conversion rate -->
                                <td class="always-hover">'.round(($fbFormSubmissions / $fbclicks) * 100, 2).'%</td>
                                <!-- avg cost per conv -->
                                <td class="always-hover">£'.round(($FBtotal / $fbFormSubmissions), 2).'</td>
								<!-- Avg cost as proporion of total spend -->
                                <td class="always-hover important">£'.round($fbFormProp/$fbFormSubmissions,2).'</td>
                                <!-- Form PROP -->
                                <td class="always-hover">£'.round($fbFormProp, 2).'</td>
                                <!-- email Clicks -->
                                <td>'.$fbEmailClicks.'</td>
                                <!-- Prop of spend on emails -->
                                <td>£' . round($fbEmailProp/$fbEmailClicks, 2) .'</td>
                                <!-- Email Proportion -->
                                <td>£'.round($fbEmailProp, 2).'</td>
                                <!-- Phone Clicks -->
                                <td>'.$fbPhoneClicks.'</td>
                                <!-- Prop of spend on emails -->
                                <td>£' . round($fbPhoneProp/$fbPhoneClicks, 2) .'</td>
                                <!-- phone prop -->
                                <td>£'.round($fbPhoneProp, 2).'</td>
                                <!-- cost per conv -->
                                <td>£'.round($FBtotal / ($fbPhoneClicks + $fbEmailClicks + $fbFormSubmissions), 2).'</td>
                            </tr>
                            <tr>';*/
                        $paidtotal = $adwordsCost; // + $FBtotal;
                        $paidGAQvisit = $GAQ_visits; //+$fbGAQvisits;
                        $paidtotalClicks = $adwordsClicks; //+$fbclicks;
                        $paidtotalFormSubs = $formSubmissions; //+$fbFormSubmissions;
                        $paidtotalEmailClicks = $emailClicks; //+$fbEmailClicks;
                        $paidtotalPhoneClicks = $phoneClicks; //+$fbPhoneClicks;
                        $totalEmailProp = $fbEmailProp; //+$adwordsemailProp;
                        $totalphoneprop = $fbPhoneProp; //+$adwordsphoneProp;
                        $totalformProp = $fbFormProp; //+$adwordsformProp;

                        $table .= '
                                <!-- Campaign -->
                                <td class="first-col">Total Paid Traffic</td>
                                <!-- Spend -->
                                <td>£'.round($paidtotal, 2).'</td>
                                <!-- clicks -->
                                <td>'.$paidtotalClicks.'</td>
                                <!-- GaQ visits -->
                                <td>'.$paidGAQvisit.'</td>
                                <!-- % of clicks -->
                                <!-- cost per GaQ Conversion  -->
                                <td>£'.round(($paidtotal / $paidGAQvisit), 2).'</td>
                                <!-- GaQ form submissions -->
                                <td class="always-hover">'.$paidtotalFormSubs.'</td>
                                <!-- GaQ Conversion rate -->
                                <td class="always-hover">'.round(($paidtotalFormSubs / $paidtotalClicks) * 100, 2).'%</td>
                                <!-- avg cost per conv -->
                                <td class="always-hover">£'.round(($paidtotal / $paidtotalFormSubs), 2).'</td>
                                <!-- Avg cost as proporion of total spend -->
                                <td class="always-hover important">£'.round($totalformProp/$paidtotalFormSubs,2).'</td>
                                <!-- Form PROP -->
                                <td class="always-hover">£'.round($totalformProp, 2).'</td>
                                <!-- email Clicks -->
                                <td>'.$paidtotalEmailClicks.'</td>
                                <!-- Prop of spend on emails -->
                                <td>£' . round($totalEmailProp/$paidtotalEmailClicks, 2) .'</td>
                                <!-- Email Proportion -->
                                <td>£'.round($totalEmailProp, 2).'</td>
                                <!-- Phone Clicks -->
                                <td>'.$paidtotalPhoneClicks.'</td>
                                <!-- Prop of spend on emails -->
                                <td>£' . round($totalphoneprop/$paidtotalPhoneClicks, 2) .'</td>
                                <!-- phone prop -->
                                <td>£'.round($totalphoneprop, 2).'</td>
                                <!-- cost per conv -->
                                <td>£'.round($paidtotal / ($paidtotalPhoneClicks + $paidtotalEmailClicks + $paidtotalFormSubs), 2).'</td>
                            </tr>';

                        $totalClicks = get_total_clicks();
                        $totalGAQ = get_total_GAQ_page_visits();
                        $totalForm = get_total_form_submissions();
                        $totalEmails = get_total_email_clicks();
                        $totalPhones = get_total_phone_clicks();
                        $totalInteractions = $totalForm+$totalEmails+$totalPhones;
                        $formProp = $totalForm / $totalInteractions * $paidtotal;


                        $organicClicks = $totalClicks-$paidtotalClicks;
                        $organicGAQ = $totalGAQ-$paidGAQvisit;
                        $organicForms = $totalForm-$paidtotalFormSubs;
                        $organicEmails = $totalEmails-$paidtotalEmailClicks;
                        $organicPhones = $totalPhones-$paidtotalPhoneClicks;

                        $table .= '
                            <tr>
                                <!-- Campaign -->
                                <td class="first-col">Organic</td>
                                <!-- Spend -->
                                <td>£-</td>
                                <!-- clicks -->
                                <td>'.$organicClicks.'</td>
                                <!-- GaQ visits -->
                                <td>'.$organicGAQ.'</td>
                                <!-- % of clicks -->
                                <!-- cost per GaQ Conversion  -->
                                <td>£0.00</td>
                                <!-- GaQ form submissions -->
                                <td class="always-hover">'.$organicForms.'</td>
                                <!-- GaQ Conversion rate -->
                                <td class="always-hover">'.round(($organicForms / $organicClicks) * 100, 2).'%</td>
                                <!-- avg cost per conv -->
                                <td class="always-hover">£0.00</td>
                                <!-- prop of total -->
                                <td class="always-hover important">£0.00</td>
                                <!-- Form PROP -->
                                <td class="always-hover">£0.00</td>
                                <!-- email Clicks -->
                                <td>'.$organicEmails.'</td>
                                <!-- Email Prop of total -->
                                <td>£0.00</td>
                                <!-- Email Proportion -->
                                <td> £- </td>
                                <!-- Phone Clicks -->
                                <td>'.$organicPhones.'</td>
                                <!-- prop of total phone -->
                                <td>£0.00</td>
                                <!-- phone prop -->
                                <td> £- </td>
                                <!-- cost per conv -->
                                <td> £- </td>
                            </tr>
                            <tr>';
                        $totalints = $totalForm+$totalEmails+$totalPhones;
                        $allformProp = ($totalForm/ $totalints) * $paidtotal;
                        $allemailProp = ($totalEmails / $totalints) * $paidtotal;
                        $allphoneProp = ($totalPhones / $totalints) * $paidtotal;

                        $table .= '
                                <!-- Campaign -->
                                <td class="first-col">Total overall</td>
                                <!-- Spend -->
                                <td>'.round($paidtotal, 2).'</td>
                                <!-- clicks -->
                                <td>'.$totalClicks.'</td>
                                <!-- GaQ visits -->
                                <td>'.$totalGAQ.'</td>
                                <!-- % of clicks -->
                                <!-- cost per GaQ Conversion  -->
                                <td>£'.round(($paidtotal / $totalGAQ), 2).'</td>
                                <!-- GaQ form submissions -->
                                <td class="always-hover">'.$totalForm.'</td>
                                <!-- GaQ Conversion rate -->
                                <td class="always-hover">'.round(($totalForm / $totalClicks) * 100, 2).'%</td>
                                <!-- avg cost per conv -->
                                <td class="always-hover">£'.round(($paidtotal / $totalForm), 2).'</td>
                                <!-- Avg cost as proporion of total spend -->
                                <td class="always-hover important">£'.round($allformProp/$totalForm,2).'</td>
                                <!-- Form PROP -->
                                <td class="always-hover">£'.round($allformProp, 2).'</td>
                                <!-- email Clicks -->
                                <td>'.$totalEmails.'</td>
                                <!-- Prop of spend on emails -->
                                <td>£' . round($allemailProp/$totalEmails, 2) .'</td>
                                <!-- Email Proportion -->
                                <td>£'.round($allemailProp, 2).'</td>
                                <!-- Phone Clicks -->
                                <td>'.$totalPhones.'</td>
                                <!-- Prop of spend on emails -->
                                <td>£' . round($allphoneProp/$totalphoneprop, 2) .'</td>
                                <!-- phone prop -->
                                <td>£'.round($allphoneProp, 2).'</td>
                                <!-- cost per conv -->
                                <td>£'.round($paidtotal / ($totalPhones + $totalEmails + $totalForm), 2).'</td>
                            </tr>
                            </tbody>
                        </table>';
                        print $table;
                        $csv;

                        $html = str_get_html($table);
                        $time = time();
                        $fp = fopen(getcwd() . "/temp/spreadsheet-$time.csv", "w");

                        foreach($html->find('tr') as $element)
                        {
                            $td = array();
                            foreach( $element->find('th') as $row)
                            {
                                $td [] = $row->plaintext;
                            }
                            fputcsv($fp, $td);

                            $td = array();
                            foreach( $element->find('td') as $row)
                            {
                                $td [] = $row->plaintext;
                            }
                            fputcsv($fp, $td);
                        }


                        fclose($fp);
                        ?>
                        <h2><a target="_blank" href="temp/spreadsheet-<?=$time?>.csv">Download as CSV File</a></h2>
                    </div>
                </div>
            </div>
        </div>
        <?php footer()?>
    </div>
</div>

</body>

<!--   Core JS Files   -->
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>



</html>
